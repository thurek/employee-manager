/*global angular*/
angular.module('employeemanager.controller', []).controller(
    'EmployeeController',
    ['$scope', '$http',
				function ($scope, $http) {
            'use strict';
            $scope.url = '/employee-manager/services/employee/';
            $scope.employees = [];
            $scope.initModel = function () {
                $scope.employee = {
                    "id": 0,
                    "firstName": '',
                    "surname": '',
                    "salary": 0,
                    "hireDate": null
                };
            };
            
            $scope.initModel();
                    
            $scope.findAllEmployees = function () {
                $http.get($scope.url, {}).then(function (response) {
                    $scope.responseData = response.data;
                });
            };

            $scope.search = function () {
                $http.post($scope.url + 'search', $scope.employee, {}).then(
                    function (response) {
                        if (response.data.constructor === Array) {
                            $scope.employees = response.data;
                        }
                    }
                );
            };

            $scope.find = function () {
                $http.get($scope.url + '/' + $scope.employee.id, {}).then(
                    function (response) {
                        $scope.employee = response.data;
                    }
                );
            };

            $scope.save = function () {
                $http.post($scope.url, $scope.employee, {}).then(
                    function (response) {
                        var employeeCopy = JSON.parse(JSON.stringify($scope.employee));
                        $scope.employees.push(employeeCopy);
                        $scope.initModel();
                    }
                );
            };

            $scope.update = function () {
                $http.put($scope.url + '/' + $scope.employee.id, $scope.employee, {}).then(
                    function (response) {
                        var employee = response.data,
                            i;
                        for (i in $scope.employees) {
                            if ($scope.employees[i].id === employee.id) {
                                $scope.employees[i] = employee;
                                break;
                            }
                        }
                    }
                );
            };

            $scope.remove = function (employee) {
                $http.delete($scope.url + '/' + employee.id, {}).then(
                    function (response) {
                        var index = $scope.employees.indexOf(employee);
                        $scope.employees.splice(index, 1);
                    }
                );
            };
         
        }
        ]
);