package org.thurek.employeemanager.service.impl;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.thurek.employeemanager.persistence.employee.Employee;
import org.thurek.employeemanager.persistence.employee.dao.EmployeeDao;

@RestController
@RequestMapping("/employee/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> findAllEmployees() {
		List<Employee> employees = employeeDao.findAll();
		if (employees.isEmpty()) {
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@RequestMapping(value = "search", method = RequestMethod.POST)
	public ResponseEntity<List<Employee>> search(@RequestBody Employee employee) {
		List<Employee> employees = employeeDao.search(employee);
		if (employees.isEmpty()) {
			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<Employee> find(@PathVariable("id") long id) {
		Employee employee = employeeDao.findById(id);
		if (employee == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Void> save(@RequestBody Employee employee, UriComponentsBuilder ucBuilder) {
		employeeDao.save(employee);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/employee/{id}").buildAndExpand(employee.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public ResponseEntity<Employee> update(@PathVariable("id") long id, @RequestBody Employee employee) {
		if (employeeDao.findById(id) == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		employeeDao.update(employee);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Employee> delete(@PathVariable("id") Long id) {
		Employee employee = employeeDao.findById(id);
		if (employee == null) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
		employeeDao.delete(employee);
		return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
	}

}
