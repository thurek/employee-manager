package org.thurek.employeemanager.persistence.employee.dao;

import java.util.List;

import org.thurek.employeemanager.persistence.employee.Employee;

public interface EmployeeDao {

	public void save(Employee employee);

	public void update(Employee employee);
	
	public void delete(Employee employee);

	public Employee findById(Long id);

	public List<Employee> findAll();
	
	public List<Employee> search(Employee employee);

}
