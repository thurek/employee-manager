package org.thurek.employeemanager.persistence.employee.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thurek.employeemanager.persistence.employee.Employee;
import org.thurek.employeemanager.persistence.employee.dao.EmployeeDao;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired(required = true)
	private SessionFactory sessionFactory;

	private HibernateTemplate hibernateTemplate;

	@PostConstruct
	public void setUp() {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Transactional(rollbackFor = Throwable.class)
	@Override
	public void save(Employee employee) {
		hibernateTemplate.save(employee);
	}

	@Transactional(rollbackFor = Throwable.class)
	@Override
	public void update(Employee employee) {
		hibernateTemplate.update(employee);
	}

	@Transactional(rollbackFor = Throwable.class)
	@Override
	public void delete(Employee employee) {
		hibernateTemplate.delete(employee);
	}

	@Transactional
	@Override
	public Employee findById(Long id) {
		return hibernateTemplate.get(Employee.class, id);
	}

	@Transactional
	@Override
	public List<Employee> findAll() {
		return hibernateTemplate.loadAll(Employee.class);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Employee> search(Employee employee) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Employee.class);

		if (employee.getFirstName() != null && !employee.getFirstName().isEmpty()) {
			criteria.add(Restrictions.like("firstName", employee.getFirstName()));
		}
		if (employee.getSurname() != null && !employee.getSurname().isEmpty()) {
			criteria.add(Restrictions.like("surname", employee.getSurname()));
		}
		criteria.addOrder(Order.asc("salary"));
		criteria.setMaxResults(10);
		return criteria.list();
	}

}
