package org.thurek.employeemanager.persistence.employee.dao;

import java.util.function.Supplier;

import org.hamcrest.core.Is;
import org.hibernate.criterion.Projections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.thurek.employeemanager.common.AbstractTransactionalSpringTest;
import org.thurek.employeemanager.persistence.employee.Employee;

public class EmployeeDaoTest extends AbstractTransactionalSpringTest {

	@Autowired
	private EmployeeDao employeeDao;

	private Supplier<Employee> employeeSupplier = () -> {
		Employee employee = new Employee();
		employee.setFirstName("John");
		employee.setSurname("Smith");
		employee.setSalary(1000);
		employee.setHireDate(java.time.LocalDateTime.now());
		return employee;
	};

	@Before
	public void setUp() {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM Employee").executeUpdate();
	}

	@Ignore
	@Test
	public void testSaveEmployee() {
		// given
		Employee employee = employeeSupplier.get();
		long countPre = (long) sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.setProjection(Projections.rowCount()).uniqueResult();
		// when
		employeeDao.save(employee);
		// then
		long countPost = (long) sessionFactory.getCurrentSession().createCriteria(Employee.class)
				.setProjection(Projections.rowCount()).uniqueResult();

		Assert.assertThat(countPost, Is.is(countPre + 1));
	}

	@Ignore
	@Test
	public void testDeleteEmployee() {
		// given
		Employee employee = employeeSupplier.get();
		// when
		employeeDao.delete(employee);
		// then
	}

	@Ignore
	@Test
	public void testUpdateEmployee() {
		// given
		Employee employee = employeeSupplier.get();
		// when
		employeeDao.update(employee);
		// then
	}

	@Ignore
	@Test
	public void testSearchEmployee() {
		// given
		Employee employee = employeeSupplier.get();
		// when
		employeeDao.search(employee);
		// then
	}

	@Ignore
	@Test
	public void testFindById() {
		// given
		long id = 0;
		// when
		employeeDao.findById(id);
		// then
	}

	@Ignore
	@Test
	public void testFindAll() {
		// given
		// when
		employeeDao.findAll();
		// then
	}

}
