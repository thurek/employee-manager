package org.thurek.employeemanager.persistence.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.thurek.employeemanager.common.AbstractTransactionalSpringTest;
import org.thurek.employeemanager.service.impl.EmployeeService;

public class EmployeeServiceTest extends AbstractTransactionalSpringTest {

	@Autowired
	private EmployeeService employeeService;

	@Ignore
	@Test
	public void testFind() {
		// given
		// when
		employeeService.find(0L);
		// then
	}

	@Ignore
	@Test
	public void testDelete() {
		// given
		// when
		employeeService.delete(0L);
		// then
	}

	@Ignore
	@Test
	public void testFindAllEmployees() {
		// given
		// when
		employeeService.findAllEmployees();
		// then
	}

	@Ignore
	@Test
	public void testSave() {
		// given
		// when
		employeeService.save(null, null);
		// then
	}

	@Ignore
	@Test
	public void testSearch() {
		// given
		// when
		employeeService.search(null);
		// then
	}

	@Ignore
	@Test
	public void testUpdate() {
		// given
		// when
		employeeService.update(0L, null);
		// then
	}

}
