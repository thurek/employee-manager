package org.thurek.employeemanager.common;

import javax.annotation.PostConstruct;

import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml", "classpath:hibernateConfig.xml"  })
@EnableTransactionManagement
@Transactional
public abstract class AbstractTransactionalSpringTest extends AbstractJUnit4SpringContextTests {

	@Autowired(required = true)
	protected SessionFactory sessionFactory;

	protected HibernateTemplate hibernateTemplate;

	@PostConstruct
	public void initialize() {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

}
